function problem5(stringArray) {
  if (Array.isArray(stringArray)) {
    return stringArray.join(" ");
  }
  return [];
}

module.exports = problem5;
