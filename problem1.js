function problem1(numString) {
  let finalNumber = "";
  let foundDollar = false;
  let commaContinous = false;
  let foundPlusOrMinus = false;
  let numbersStarted = false;
  let foundDot = false;
  if (typeof numString != "string") return 0;
  for (let index = 0; index < numString.length; index++) {
    switch (index) {
      case 0:
        if (numString[index] == "+" || numString[index] == "-") {
          foundPlusOrMinus = true;
          finalNumber += numString[index];
        } else if (numString[index] == "$") {
          foundDollar = true;
          foundPlusOrMinus = true;
        } else if (Number.isInteger(+numString[index])) {
          foundDollar = true;
          foundPlusOrMinus = true;
          finalNumber += numString[index];
          numbersStarted = true;
        } else {
          return 0;
        }
        break;
      case 1:
        if (numString[index] == "$") {
          if (foundDollar) {
            return 0;
          }
          foundDollar = true;
        } else if (Number.isInteger(+numString[index])) {
          foundDollar = true;
          foundPlusOrMinus = true;
          finalNumber += numString[index];
        } else if (numString[index] == "," || numString == ".") {
          if (numbersStarted) {
            if (numString[index] == ",") {
              commaContinous = true;
            } else {
              foundDot = true;
            }
          } else {
            return 0;
          }
        } else {
          return 0;
        }
        break;
      default:
        if (numString[index] == ",") {
          if (commaContinous) {
            return 0;
          } else {
            commaContinous = true;
          }
        } else if (numString[index] == ".") {
          if (foundDot) {
            return 0;
          } else {
            finalNumber += numString[index];
            foundDot = true;
          }
        } else if (Number.isInteger(+numString[index])) {
          finalNumber += numString[index];
          commaContinous = false;
        } else {
          return 0;
        }
        break;
    }
  }
  return finalNumber;
}

module.exports = problem1;
