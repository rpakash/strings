function problem3(date) {
  let dataArray = (date + "").split("/");
  let months = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];
  if (dataArray.length === 3) {
    return months[dataArray[1] - 1];
  }
  return "";
}

module.exports = problem3;
