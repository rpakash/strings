function problem2(ipAddress) {
  if (typeof ipAddress !== "string") {
    return [];
  }
  let ipArray = ipAddress.split(".");
  if (ipArray.length === 4) {
    for (let index = 0; index < 4; index++) {
      if (!Number.isInteger(Number(ipArray[index]))) {
        return [];
      }
    }
  } else {
    return [];
  }
}

module.exports = problem2;
