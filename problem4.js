function problem4(nameObject) {
  let fullNameArray = [];
  if (typeof nameObject == "object") {
    for (const partialName of Object.values(nameObject)) {
      let name = partialName.toLowerCase();
      name = name.charAt(0).toUpperCase() + name.slice(1);
      fullNameArray.push(name);
    }
  }
  return fullNameArray.join(" ");
}

module.exports = problem4;
